package com.danmamaliga.photoapp.discovery.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.web.SecurityFilterChain;

@EnableWebSecurity
@Configuration
public class WebSecurity {

	@Bean
	public SecurityFilterChain configure(HttpSecurity http) throws Exception {

		http.csrf(h -> h.disable()).authorizeHttpRequests(c -> c.anyRequest().authenticated())
				.httpBasic(Customizer.withDefaults());

		return http.build();
	}

}
